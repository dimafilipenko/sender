<?php namespace App\Filters;

use App\ThirdParty\Bridge;
use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class Auth implements FilterInterface
{
    use Bridge;
    public function before(RequestInterface $request)
    {

        // Do something here
        $profile = null;
        $user = session()->get("user");

        if ($user) {
            $profile = $this->profile();
        }
        if (!session()->get('isLoggedIn') || !$profile) {
            session()->destroy();
            return redirect()->to(base_url('/'));
        } else {
            session()->set("user", (object)$profile);
        }
        return false;
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response)
    {
        // Do something here
    }
}
