<?php namespace Config;

// Create a new instance of our RouteCollection class.

$routes = Services::routes(true);

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('AuthController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'AuthController::index');
$routes->get('logout', 'AuthController::logout');
//$routes->match(['get', 'post'], 'register', 'UsersController::register', ['filter' => 'noauth']);
$routes->get('dashboard', 'Dashboard::index', ['filter' => 'auth']);

$routes->get('accounts', 'UsersController::index', ['filter' => 'admin']);
$routes->match(['get', 'post'], 'account/(:num)', 'UsersController::edit/$1', ['filter' => 'admin']);
$routes->match(['get', 'post'], 'create-account/', 'UsersController::store', ['filter' => 'admin']);
$routes->put('profile', 'UsersController::edit/$1', ['filter' => 'auth']);
$routes->get('profile', 'UsersController::showProfile', ['filter' => 'auth']);
$routes->get('account/delete/(:num)', 'UsersController::delete/$1', ['filter' => 'admin']);

$routes->get('create-email', 'EmailsController::create', ['filter' => 'auth']);
$routes->post('create-email', 'EmailsController::send', ['filter' => 'auth']);
$routes->get('create-sms', 'SmsController::create', ['filter' => 'auth']);
$routes->post('create-sms', 'SmsController::send', ['filter' => 'auth']);
$routes->get('create-viber', 'ViberController::create', ['filter' => 'auth']);
$routes->post('create-viber', 'ViberController::send', ['filter' => 'auth']);


$routes->get('databases', 'UsersDBController::index', ['filter' => 'admin']);
$routes->get('database', 'UsersDBController::show', ['filter' => 'auth']);
$routes->get('database/(:num)', 'UsersDBController::showById/$1', ['filter' => 'admin']);
//$routes->match(['get', 'post'], 'database/edit/(:num)/(:num)', 'UsersDBController::editById/$1/$2', ['filter' => 'admin']);
//$routes->match(['get', 'post'], 'database/edit/(:num)', 'UsersDBController::edit/$1', ['filter' => 'auth']);
$routes->match(['get', 'post'], 'create-database-item', 'UsersDBController::store', ['filter' => 'auth']);
$routes->match(['get', 'post'], 'create-database-item/(:num)', 'UsersDBController::storeByAdmin/$1', ['filter' => 'admin']);

$routes->get('database/delete/(:num)/(:num)', 'UsersDBController::deleteById/$1/$2', ['filter' => 'admin']);
$routes->get('database/delete/(:num)', 'UsersDBController::delete/$1', ['filter' => 'auth']);
$routes->get('statuses', 'StatusesController::index', ['filter' => 'auth']);


$routes->get('gateways', 'GatewaysController::index', ['filter' => 'admin']);
$routes->get('create-gateway', 'GatewaysController::create', ['filter' => 'admin']);
$routes->post('create-gateway', 'GatewaysController::store', ['filter' => 'admin']);
$routes->get('gateway/(:num)', 'GatewaysController::edit/$1', ['filter' => 'admin']);
$routes->post('gateway/(:num)', 'GatewaysController::update/$1', ['filter' => 'admin']);
$routes->get('gateway/delete/(:num)', 'GatewaysController::delete/$1', ['filter' => 'admin']);

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
