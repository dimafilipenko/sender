<?php namespace App\Controllers;

use App\ThirdParty\Bridge;

class UsersController extends BaseController
{
    use Bridge;


    public function index()
    {

        try {
            $data["users"] = $this->getUsers();
            return view("/pages/users/index.twig", $data);
        } catch (\Exception $e) {

        }

    }


    public function store()
    {
        $data["gateways"] = $this->getGateways();
        if ($this->request->getMethod() == "post") {
            $rules = [
                "email" => "required|min_length[6]|max_length[50]|valid_email",
//                "password" => "required|min_length[8]|max_length[255]",
            ];

            $errors = [
                "password" => [
                    "validateUser" => "Email or Password don\"t match"
                ]
            ];

            if (!$this->validate($rules, $errors)) {
                $data["validation"] = $this->validator;
            }

            $fields = [
                "body" => [
                    "email" => $this->request->getVar("email"),
                    "name" => $this->request->getVar("name"),
                    "role" => $this->request->getVar("role"),
                    "webhook_url" => $this->request->getVar("webhook_url"),
                    "password" => $this->request->getVar("password"),
                ]
            ];

            try {
                $this->createUser($fields);
            } catch (\Exception $e) {
                return redirect()->back()->with("errors", $e->getMessage());

            }

            return redirect()->to(base_url("/accounts"));
        }

        return view("/pages/users/create.twig", $data);
    }


    public function edit($id)
    {

        $data = [];


        if (!$id)
            $id = session()->get("user")->id;
        if ($this->request->getMethod() == "post") {
            $gateways = $this->getGateways();
            if ($this->request->getVar("refresh")) {
                try {
                    $user = $this->refreshToken($id);
                    if ($id == session()->get("user")->id)
                        session()->set('user', (object)$user);
                    return redirect()->back();
                } catch (\Exception $e) {
                }
            } else {
                $rules = [
                    "email" => "required|min_length[6]|max_length[50]|valid_email",
//                "password" => "required|min_length[8]|max_length[255]",
                ];

                $errors = [
                    "password" => [
                        "validateUser" => "Email or Password don\"t match"
                    ]
                ];
                if (!$this->validate($rules, $errors)) {
                    $data["validation"] = $this->validator;
                } else {
                    // Get Fields from form to update user
                    $fields = [
                        "body" => [
                            "email" => $this->request->getVar("email"),
                            "name" => $this->request->getVar("name"),
                            "role" => $this->request->getVar("role"),
                            "webhook_url" => $this->request->getVar("webhook_url"),
                            "password" => $this->request->getVar("password"),
                        ]
                    ];
                    $selectedGatewaysIds = [];
                    foreach ($gateways as $gateway) {
                        $value = $this->request->getVar('gateway' . $gateway->id);
                        if ($value)
                            array_push($selectedGatewaysIds, $gateway->id);
                    }

                    $fields["body"]["gatewaysIds"] = join(",", $selectedGatewaysIds);

                    if ($id == session()->get("user")->id) {
                        $this->updateProfile($fields);

                        return redirect()->to(base_url("profile"));
                    } else {
                        $this->updateUser($id, $fields);
                        return redirect()->to(base_url("account/$id"));
                    }
                }
            }

        }


        //Check if user get his info by himself or it`s admin
        $gateways = $this->getGateways();
        if ($id == session()->get("user")->id) {
            $data["profile"] = $this->profile();
        } else {
            $data["profile"] = $this->user($id);
        }
        $userGateways = $data["profile"]["gtws"];

        if ($data["profile"]) {
            foreach ($gateways as $item) {
                foreach ($userGateways as $gateway) {
                    if ($item->id === $gateway->id) {
                        $item->active = true;
                    }
                }

            }
            $data["gateways"] = $gateways;
            return view("pages/users/profile.twig", $data);
        } else
            return redirect()->to(base_url());
    }


    public function showProfile()
    {
        return $this->edit("");
    }

    public function delete($id)
    {
        $this->deleteUser($id);
        return redirect()->back();
    }

    //--------------------------------------------------------------------

}
