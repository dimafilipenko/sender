<?php


namespace App\Controllers;


use App\ThirdParty\Bridge;
use CodeIgniter\Controller;

class StatusesController extends Controller
{
    use Bridge;

    public function index()
    {
        $data = $this->getStatus();
        $emails = $data['emails'];
        $getCountOfSuccess = function ($item) {
            // returns whether the input integer is odd
            if ($item->status == "delivered")
                return $item;
        };
        foreach ($emails as $key => $email) {
            $email->delivered = count(array_filter($email->email_statuses, $getCountOfSuccess));
            $email->count = count($email->email_statuses);
        }


        $this->makeDataForTable($data['sms']);
        $this->makeDataForTable( $data['viber']);
        return view("pages/statuses/index.twig", $data);

    }
    public function makeDataForTable(&$table){
        foreach ($table as $k => $item) {
            $item->count = count($item->message_statuses);
            $item->cost = 0;
            $item->delivrd = 0;
            foreach ($item->message_statuses as $value) {
                if ($value->status == "delivrd") {
                    $item->delivrd++;
                }
                $item->cost += $value->cost;
            }
        }
    }
}