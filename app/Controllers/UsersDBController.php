<?php


namespace App\Controllers;


use App\ThirdParty\Bridge;
use CodeIgniter\Controller;

class UsersDBController extends Controller
{

    use Bridge;

    public function index()
    {
        $data['users'] = $this->getUsersDb();
        return view("pages/usersDb/index.twig", $data);
    }

    public function store()
    {
        if ($this->request->getMethod() == "get")
            return view("pages/usersDb/create.twig");
        elseif ($this->request->getMethod() == "post") {
            $fields = [
                "body" => [
                    "email" => $this->request->getVar("email"),
                    "name" => $this->request->getVar("name"),
                    "phone" => $this->request->getVar("phone"),
                ]
            ];
            try {
                $response = $this->createUserDBItem($fields);
                return redirect()->to(base_url("database"));
            } catch (\Exception $e) {
                return redirect()->to(base_url("database"))->with("err", $e->getMessage());
            }

        }
    }

    public function storeByAdmin($id)
    {
        if ($this->request->getMethod() == "get")
            return view("pages/usersDb/create.twig", ["id" => $id]);
        elseif ($this->request->getMethod() == "post") {
            $fields = [
                "body" => [
                    "email" => $this->request->getVar("email"),
                    "name" => $this->request->getVar("name"),
                    "phone" => $this->request->getVar("phone"),
                ]
            ];
            try {
                $this->createUserDBItemByAdmin($id, $fields);
            } catch (\Exception $exception) {
                $eer = $exception->getMessage();
                return redirect()->to(base_url("database/$id"))->with("errors", $eer);

            }
            return redirect()->to(base_url("database/$id"));

        }
    }

    public function show()
    {
        $data = [];
        try {
            $data['profile'] = $this->getUserDb();

        } catch (\Exception $e) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
        return view("pages/usersDb/profileDb.twig", $data);

    }

    public function edit($id)
    {
//        if ($this->request->getMethod() == "post") {
//
//        }
//        return view("pages/usersDb/profileDb.twig", $data);
    }

    public function delete($id)
    {
        try {
            $this->deleteAccountDbUser($id);

        } catch (\Exception $e) {
            return redirect()->to(base_url("dashboard"))->with("err", $e->getMessage());
        }
        return redirect()->to(base_url("database"));

    }

    public function showById($account_id)
    {
        $data = [];
        try {
            $data['profile'] = $this->getUserDbById($account_id);

//            dd($data);
        } catch (\Exception $e) {
            return redirect()->to(base_url("dashboard"));
        }

        return view("pages/usersDb/profileDb.twig", $data);
    }

    public function editById($account_id, $id)
    {

    }


    public function deleteById($account_id, $id)
    {
        try {
            $this->deleteAccountDbUserById($account_id, $id);
        } catch (\Exception $e) {
            return redirect()->to(base_url("dashboard"))->with("err", $e->getMessage());
        }
        return redirect()->to(base_url("database/$account_id"));


    }
}