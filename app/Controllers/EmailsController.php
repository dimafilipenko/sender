<?php


namespace App\Controllers;


use App\ThirdParty\Bridge;
use CodeIgniter\Controller;

class EmailsController extends Controller
{

    use Bridge;

    public function create()
    {

        // Email Templates from app/Views/emails
        helper('filesystem');
        $path = APPPATH . 'Views/emails/';
        $map = directory_map($path);
        $templates = [];
        foreach ($map as $value) {
            $name = pathinfo($path . $value, PATHINFO_FILENAME); // outputs html
            $templates[$name] = file_get_contents($path . $value);
        }
        //--------------------------------
        //GET USER DATABASE
        $users = $this->getUserDB();
        $profile = $this->profile();
        $data = [
            "templates" => $templates,
            "users" => $users["database"],
            "profile" => $profile,
        ];

        return view('pages/emails/create.twig', $data);
    }

    public function send()
    {
        //get template name from input
        helper('filesystem');
        $path = APPPATH . 'Views/emails/';
        $map = directory_map($path);
        $template = null;
        foreach ($map as $value) {
            $name = pathinfo($path . $value, PATHINFO_FILENAME); // outputs html
            if ($this->request->getVar("template") == $name) {
                $template = $value;
            }
        }
        //-----------------------------

        //START to get Emails For sending
        $resultEmails = [];
        $users = $this->getUserDB()["database"];
        foreach ($users as $key => $user) {
            if ($this->request->getVar("user" . $user->id)) {
                $email = $user->email;
                if (filter_var($email, FILTER_VALIDATE_EMAIL))
                    array_push($resultEmails, $email);
            }
        }
        $resultEmails = join(',', $resultEmails);
        //------------------------------

//        dd($resultEmails);

        //Other field (title,emailFrom,gateway_name)
        $title = $this->request->getVar("title");
        $text = $this->request->getVar("text");
        $emailFrom = $this->request->getVar("emailFrom");
        $gateway = $this->request->getVar("gateway");

        //-------------------------------
        //KIEV TIMEZONE


        //TODO: Attachments if needed
        $fields = [
            "body" => [
                "gateway_name" => $gateway,
                "text" => "$text",
                "title" => "$title",
                "html" => "$text",
                "emailFrom" => "dimafilipenko2002@gmail.com",
                "emailTo" => $resultEmails
            ]
        ];
        if ($this->request->getVar("scheduled")) {
            $sendAt = $this->request->getVar("sendAt");
            $dateTimeKiev = date_create($sendAt, timezone_open("Europe/Kiev"));
            $fields['body']["sendAt"] = $dateTimeKiev->format(DATE_ATOM);
        }
        if ($template)
            $fields['body']["html"] = view('emails/' . $template);

        try {
            $req = $this->sendMessage($fields);

        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        return redirect()->to("statuses");

//        echo($this->request->getBody());
    }

    public function get()
    {

    }

    public function info()
    {

    }
}