<?php
namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use App\Helpers\Helpers;
class BaseController extends Controller
{
    protected $helpers = [];
    protected $twig;

    // protected $helper = [];
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);

        $appPaths = new \Config\Paths();
        $appViewPaths = $appPaths->viewDirectory;

        $loader = new \Twig\Loader\FilesystemLoader($appViewPaths);

        $this->twig = new \Twig\Environment($loader);


    }
}