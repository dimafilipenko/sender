<?php


namespace App\Controllers;


use App\ThirdParty\Bridge;
use CodeIgniter\Controller;

class GatewaysController extends Controller
{
    use Bridge;

    public function index()
    {

        $data["gateways"] = $this->getGateways();

        return view("pages/gateways/index.twig", $data);
    }

    public function create()
    {
        $data = [];
        //TODO make this data in database
        $data["types"] = ["email", "sms", "viber"];
        $data["api_names"] = ["send_grid", "intel_tele", "smtp"];
        return view("pages/gateways/create.twig", $data);

    }

    public function delete($id)
    {

        $this->deleteGateway($id);
        return redirect()->to(base_url("gateways"));

    }

    public function edit($id)
    {

        //TODO make this data in database
        $data["types"] = ["email", "sms", "viber"];
        $data["api_names"] = ["send_grid", "intel_tele", "smtp"];
        try {
            $data["gateway"] = $this->getGateway($id);
        } catch (\Exception $exception) {
            return redirect()->back();
        }

        return view("pages/gateways/edit.twig", $data);

    }

    public function update($id)
    {

        $name = $this->request->getVar("name");
        $api_name = $this->request->getVar("api_name");
        $type = $this->request->getVar("type");
        $auth = $this->request->getVar("auth");
        $fields = [
            "body" => [
                "name" => $name,
                "api_name" => $api_name,
                "type" => $type,
                "auth" => $auth,
            ]
        ];
        $this->updateGateway($id, $fields);
        return redirect()->to(base_url("gateways"));

    }

    public function store()
    {
        $name = $this->request->getVar("name");
        $api_name = $this->request->getVar("api_name");
        $type = $this->request->getVar("type");
        $auth = $this->request->getVar("auth");


        $fields = [
            "body" => [
                "name" => $name,
                "api_name" => $api_name,
                "type" => $type,
                "auth" => $auth,
            ]
        ];
        try {
            $this->createGateway($fields);

        } catch (\Exception $exception) {
            return redirect()->back()->with("errors", $exception->getMessage());
        }
        return redirect()->to(base_url("gateways"));

    }

}