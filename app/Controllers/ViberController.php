<?php


namespace App\Controllers;


use App\ThirdParty\Bridge;
use CodeIgniter\Controller;

class ViberController extends Controller
{

    use Bridge;

    public function create()
    {

        // Email Templates from app/Views/emails

        //--------------------------------
        //GET USER DATABASE
        $users = $this->getUserDB();
        $profile = $this->profile();


        $data = [
            "users" => $users["database"],
            "profile" => $profile
        ];

        return view('pages/viber/create.twig', $data);
    }

    public function send()
    {
        //get template name from input

        //-----------------------------

        //START to get Emails For sending
        $resultPhones = [];
        $users = $this->getUserDB()["database"];
        foreach ($users as $key => $user) {
            if ($this->request->getVar("user" . $user->id)) {
                $phone = $user->phone;
                array_push($resultPhones, $phone);
            }
        }
        $resultPhones = join(',', $resultPhones);
        //------------------------------


        $title = $this->request->getVar("title");
        $text = $this->request->getVar("text");
        $gateway = $this->request->getVar("gateway");
        $button_text= $this->request->getVar("button_text");
        $button_link= $this->request->getVar("button_link");
        $image= $this->request->getVar("image");
        //-------------------------------
        //KIEV TIMEZONE


        //TODO: Attachments if needed
        $fields = [
            "body" => [
                "gateway_name" => "$gateway",
                "text" => "$text",
                "name" => "$title",
                "button_text" => "$button_text",
                "button_link" => "$button_link",
                "image" => "$image",
                "number"=>$resultPhones
            ]
        ];
        if ($this->request->getVar("scheduled")) {
            $sendAt = $this->request->getVar("sendAt");
            $dateTimeKiev = date_create($sendAt, timezone_open("Europe/Kiev"));
            $fields['body']["sendAt"] = $dateTimeKiev->format(DATE_ATOM);
        }

        try {
            $req = $this->sendMessage($fields);

        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        return redirect()->to("statuses");

//        echo($this->request->getBody());
    }

    public function get()
    {

    }

    public function info()
    {

    }
}