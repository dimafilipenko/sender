<?php


namespace App\Controllers;


use App\Models\UserModel;
use App\ThirdParty\Bridge;
use Exception;

class AuthController extends BaseController
{

    use Bridge;

    public function index()
    {
        $data = [];
        helper(['form']);
        if ($this->request->getMethod() == 'post') {
            //let's do the validation here

            if (!$this->validate(UserModel::$validationAuthRules, UserModel::$validationAuthErrors))
                $data['validation'] = $this->validator;
            else {
                $fields = [
                    "body" => [
                        "email" => $this->request->getVar('email'),
                        "password" => $this->request->getVar('password')
                    ]
                ];

                try {
//                    dd($fields);
                    $response = $this->login($fields);
//                    dd("OK");
                } catch (Exception $e) {
//                    dd($e);
                    return redirect()->to(base_url('/'), 401)->with("errors", $e->getMessage());
                }
                $this->setUserSession($response);
                return redirect()->to('dashboard');
            }
        }
        return view('login.twig', $data);
    }

    private function setUserSession($response)
    {
        $response["isLoggedIn"] = true;
        session()->set($response);
        return true;
    }

    public function logout()
    {
        session()->destroy();
        return redirect()->to(base_url('/'));
    }
}