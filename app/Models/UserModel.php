<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $validationRules    = [
        'username'     => 'required|alpha_numeric_space|min_length[3]',
        'email'        => 'required|valid_email|is_unique[users.email]',
        'password'     => 'required|min_length[8]',
        'pass_confirm' => 'required_with[password]|matches[password]'
    ];
    static  $validationAuthRules = [
        'email' => 'required|min_length[6]|max_length[50]|valid_email',
        'password' => 'required|min_length[6]|max_length[255]',
    ];

    static  $validationAuthErrors = [
        'password' => ['validateUser' => 'Email or Password don\'t match']
    ];
    protected $validationMessages = [
        'email'        => [
            'is_unique' => 'Sorry. That email has already been taken. Please choose another.'
        ]
    ];
}