<?php

namespace App\Helpers;

class Helpers
{

    public function asset($path)
    {
        return base_url($path);
    }

    public function url($path)
    {
        return base_url($path);
    }

    public function lang(array $arg = [])
    {
        return call_user_func_array('lang', (array)$arg);
    }

    public function getLocale()
    {
        return $session = session()->get('lang');
    }

    public function session()
    {
        return session();
    }

    public function set_value($field)
    {
        return set_value($field);
    }

    public function uri()
    {
        return service('uri');
    }
}