<?php namespace App\ThirdParty;

use CodeIgniter\Database\Exceptions\DatabaseException;
use Exception;
use GuzzleHttp\Client;

trait Bridge
{

    private $base_url = "https://bridge.dev.smartpointlab.com";


    /**
     * Class constructor.
     *
     * @param array $fields Array containing the necessary params.
     *    $params = [
     *      'email'     => (string) DB email. Required.
     *      'password'     => (string) DB password. Required.
     *    ]
     * @return array
     * @throws Exception
     */
    public function login($fields)
    {
        return $this->request("POST", "/api/login", $fields);

    }

    /**
     * @param $id
     * @param $fields
     * @return array
     */
    public function user($id, $fields = [])
    {
        try {
            return $this->request("GET", "/api/source_account/$id", $fields);

        } catch (Exception $exception) {
            throw new DatabaseException();
        }

    }

    /**
     * @param $fields
     * @return array
     */
    public function profile($fields = [])
    {
        try {
            return $this->request("GET", "/api/source_account/", $fields);
        } catch (Exception $exception) {
            throw new DatabaseException();
        }
    }

    /**
     * @param $fields
     * @return array
     */
    public function updateProfile($fields)
    {
        try {
            return $this->request("PUT", "/api/source_account/", $fields);
        } catch (Exception $exception) {
            throw new DatabaseException();
        }
    }

    /**
     * @return array
     */
    public function getUsers()
    {
        try {
            return $this->request("GET", "/api/source_accounts");
        } catch (Exception $exception) {
            throw new DatabaseException();
        }


    }

    /**
     * @param $id
     * @param $fields
     * @return array
     */
    public function updateUser($id, $fields)
    {
        try {
            return $this->request("PUT", "/api/source_account/$id", $fields);
        } catch (Exception $exception) {
            throw new DatabaseException();
        }

    }

    /**
     * @param $id
     * @param $fields
     * @return array
     */
    public function deleteUser($id, $fields = [])
    {
        try {
            return $this->request("DELETE", "/api/source_account/$id", $fields);
        } catch (Exception $exception) {
            throw new DatabaseException();
        }

    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function refreshToken($id)
    {
        try {
            return $this->request("POST", "/api/source_account/$id/api_key_refresh", []);
        } catch (Exception $exception) {
            throw new DatabaseException();
        }
    }

    /**
     * @param $fields
     * @return array
     * @throws Exception
     */
    public function createUser($fields)
    {
        return $this->request("POST", "/api/source_account", $fields);
    }

    /**
     * @param $fields
     * @return array
     */
    public function getGateways($fields = [])
    {
        return $this->request("GET", "/api/gateways", $fields);
    }

    public function getGateway($id)
    {
        return $this->request("GET", "/api/gateway/$id");
    }

    /**
     * @param $fields
     * @return array
     */
    public function deleteGateway($id, $fields = [])
    {
        return $this->request("DELETE", "/api/gateway/$id", $fields);
    }

    public function createGateway($fields = [])
    {
        return $this->request("POST", "/api/gateway/", $fields);
    }

    public function updateGateway($id, $fields = [])
    {
        return $this->request("PUT", "/api/gateway/$id", $fields);
    }

    public function getUserDb($fields = [])
    {
        return $this->request("GET", "/api/account_db", $fields);
    }

    public function getUserDbById($id, $fields = [])
    {
        return $this->request("GET", "/api/account_db/$id", $fields);
    }

    public function getUsersDb($fields = [])
    {
        return $this->request("GET", "/api/accounts_db", $fields);

    }

    public function createUserDBItem($fields = [])
    {
        return $this->request("POST", "/api/account_db/", $fields);

    }

    public function createUserDBItemByAdmin($id, $fields = [])
    {
        return $this->request("POST", "/api/account_db/$id", $fields);

    }

    public function sendMessage($fields)
    {
        return $this->request("POST", "/api/message/send", $fields);
    }

    public function getStatus($fields = [])
    {
        return $this->request("GET", "/api/messages/getStatus", $fields);
    }

    public function deleteAccountDbUserById($account_id, $id)
    {
        return $this->request("DELETE", "/api/account_db/$account_id/$id");

    }

    public function deleteAccountDbUser($id)
    {
        return $this->request("DELETE", "/api/account_db/$id");

    }

    /**
     * @param $method
     * @param $uri
     * @param array $data
     * @return array
     * @throws Exception
     */
    private function request($method, $uri, $data = [])
    {

        $client = new Client();

        $token = session()->get("accessToken");

        $headers = [
            "Accept" => "application/json",
        ];
        if ($token)
            $headers["Authorization"] = "Bearer " . $token;
        $options = [
            'verify' => false,
            "headers" => $headers,
            'force_ip_resolve' => 'v4',

        ];
        if (isset($data["body"]))
            $options["form_params"] = $data["body"];
        if (isset(session()->get("user")->api_key))
            $options["query"] = [
                "api_key" => session()->get("user")->api_key
            ];

        try {
            $response = $client->request($method, $this->base_url.$uri, $options);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            throw new Exception($e->getResponse()->getBody()->getContents());
        }

        return (array)\GuzzleHttp\json_decode((string)$response->getBody());
    }

}