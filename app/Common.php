<?php

/**
 * The goal of this file is to allow developers a location
 * where they can overwrite core procedural functions and
 * replace them with their own. This file is loaded during
 * the bootstrap process and is called during the frameworks
 * execution.
 *
 * This can be looked at as a `master helper` file that is
 * loaded early on, and may also contain additional functions
 * that you'd like to use throughout your entire application
 *
 * @link: https://codeigniter4.github.io/CodeIgniter4/
 */
use App\Helpers\Helpers;

function view($tpl, $data = [])
{

    $appPaths = new \Config\Paths();
    $appViewPaths = $appPaths->viewDirectory;

    $loader = new \Twig\Loader\FilesystemLoader($appViewPaths);

    $twig = new \Twig\Environment($loader,[
        'debug' => true,

    ]);
    $twig->addExtension(new \Twig\Extension\DebugExtension());

    $twig->addGlobal('helper', (new Helpers()));
    try {
        return $twig->render($tpl, $data);
    } catch (\Twig\Error\LoaderError $e) {
        throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
    } catch (\Twig\Error\RuntimeError $e) {
        throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);

    } catch (\Twig\Error\SyntaxError $e) {
        throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
    }
}
